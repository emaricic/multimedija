import { Fragment } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import { createGlobalStyle } from "styled-components";
import { Routes, Route } from "react-router-dom";
import Home from "./components/home";


const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', 'Square Peg',
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`;

const App = () => {
  return (
    <div className="App" style={{ scrollBehavior: "smooth" }}>
      <Fragment>
        <CssBaseline />
        <GlobalStyle />
        <Routes>
          <Route path="/" element={<Home />} />
        </Routes>
      </Fragment>
    </div>
  );
};

export default App;
