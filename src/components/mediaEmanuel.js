import React from "react";
import EmiVideo from "../assets/video/videio.mp4";
import EmiAudio from "../assets/audio/audio1.mp3";

import { Card } from "@mui/material";
import ReactPlayer from "react-player";
const MediaEmanuel = () => {
  return (
    <Card
      sx={{
        width: "100%",
        overflow: "hidden",
        padding: "16px",
        display: "flex",
        flexWrap: "wrap",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        gap: "16px",
      }}
    >
      <ReactPlayer
        style={{ maxWidth: "600px" }}
        controls
        width="100%"
        height={"40vw"}
        url={EmiVideo}
      />
      <ReactPlayer
        style={{ maxWidth: "600px" }}
        width="100%"
        height={"40px"}
        controls
        url={EmiAudio}
      />
    </Card>
  );
};

export default MediaEmanuel;
