import React, { useState } from "react";
import {
  Box,
  Typography,
  Divider,
  Card,
  Link,
  List,
  ListItem,
} from "@mui/material";

import EmanuelImg from "../assets/images/emanuel1.jpg";
import EmiInsta from "../assets/images/emimaricic_qr.bmp";
import EmiFacebook from "../assets/images/emifacebook.gif";

import Modal from "react-modal";

import MediaEmanuel from "./mediaEmanuel";

const Emanuel = () => {
  const [open, setOpen] = useState(false);
  const [openInsta, setOpenInsta] = useState(false);
  const [openFacebook, setOpenInstaFacebook] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleOpenInsta = () => setOpenInsta(true);
  const handleCloseInsta = () => setOpenInsta(false);
  const handleOpenFacebook = () => setOpenInstaFacebook(true);
  const handleCloseFacebook = () => setOpenInstaFacebook(false);

  return (
    <Box
      className="emanuel"
      id="emanuel"
      sx={{
        width: "100%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginTop: "20px",
        marginBottom: "20px",
      }}
    >
      <Typography
        style={{
          fontWeight: "bold",
          fontSize: "4vw",
          fontFamily: "Indie Flower",
          alignSelf: "center",
        }}
      >
        Emanuel Maričić
      </Typography>
      <Box
        variant="outlined"
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          flexWrap: "wrap",
          gap: "16px",
          marginBottom: "20px",
        }}
      >
        <Card
          sx={{
            flex: "1 1 200px",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            padding: "16px",
            boxShadow: 4,
          }}
        >
          <img
            onClick={handleOpen}
            src={EmanuelImg}
            style={{
              width: "100%",
              maxWidth: "400px",
              height: "auto",
              borderRadius: "8px",
            }}
            alt=""
          />
          <Modal
            closeTimeoutMS={200}
            contentLabel="Example Modal"
            isOpen={open}
            onRequestClose={handleClose}
            style={{
              content: {
                height: "fit-content",
                display: "flex",
                justifyContent: "center",
                backgroundColor: "rgb(0, 0, 0, 0.50)",
              },
              overlay: {
                backgroundColor: "rgb(0, 0, 0, 0.75)",
              },
            }}
          >
            <img
              style={{
                width: "100%",
                maxWidth: "500px",
                maxHeight: "600px",
                alignSelf: "center",
                borderRadius: "16px",
              }}
              src={EmanuelImg}
              alt=""
            />
          </Modal>

          <Box
            sx={{
              width: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-around",
              flexWrap: "wrap",
              pading: "8px",
            }}
          >
            <img
              onClick={handleOpenInsta}
              src={EmiInsta}
              style={{ width: "100%", maxWidth: "100px", height: "auto" }}
              alt=""
            />

            <Modal
              closeTimeoutMS={200}
              contentLabel="Example Modal"
              isOpen={openInsta}
              onRequestClose={handleCloseInsta}
              style={{
                content: {
                  height: "fit-content",
                  display: "flex",
                  justifyContent: "center",
                  backgroundColor: "rgb(0, 0, 0, 0.50)",
                },
                overlay: {
                  backgroundColor: "rgb(0, 0, 0, 0.75)",
                },
              }}
            >
              <img
                style={{
                  width: "100%",
                  maxWidth: "400px",
                  maxHeight: "400px",
                  alignSelf: "center",
                }}
                src={EmiInsta}
                alt=""
              />
            </Modal>
            <img
              onClick={handleOpenFacebook}
              src={EmiFacebook}
              style={{ width: "100%", maxWidth: "100px", height: "auto" }}
              alt=""
            />
            <Modal
              closeTimeoutMS={200}
              contentLabel="Example Modal"
              isOpen={openFacebook}
              onRequestClose={handleCloseFacebook}
              style={{
                content: {
                  height: "fit-content",
                  display: "flex",
                  justifyContent: "center",
                  backgroundColor: "rgb(0, 0, 0, 0.50)",
                },
                overlay: {
                  backgroundColor: "rgb(0, 0, 0, 0.75)",
                },
              }}
            >
              <img
                style={{
                  width: "100%",
                  maxWidth: "400px",
                  maxHeight: "400px",
                  alignSelf: "center",
                  borderRadius: "64px",
                }}
                src={EmiFacebook}
                alt=""
              />
            </Modal>
          </Box>
        </Card>

        <Card
          style={{
            boxShadow: 40,
            flex: "1 1 200px",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            padding: "16px",
          }}
        >
          <Typography
            style={{
              fontWeight: "bold",
              fontSize: "2.5vw",
              fontFamily: "Indie Flower",
            }}
          >
            Informacije:
          </Typography>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Spol: muški
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Mjesto i datum rođenja:{" "}
            <Link href="https://www.osijek.hr">Osijek</Link>, 24. prosinca 1999.
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Mjesto prebivališta:{" "}
            <Link href="https://www.djakovo.hr">Đakovo</Link>, Hrvatska
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            E-mail adresa: emi_maricic@hotmail.com
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Kontakt: 095 806 9964
          </Typography>
          <Typography style={{ fontSize: "1.5vw" }}>Obrazovanje:</Typography>
          <List style={{ padding: 0, margin: 0 }}>
            <ListItem style={{ paddingTop: 4 }}>
              <Typography style={{ fontSize: "1.5vw" }}>
                <Link href="http://os-igkovacic-dj.skole.hr">
                  Osnovna škola Ivana Gorana Kovačića
                </Link>{" "}
                Đakovo
              </Typography>
            </ListItem>
            <ListItem style={{ paddingTop: 4 }}>
              <Typography style={{ fontSize: "1.5vw" }}>
                <Link href="https://strukovna.com">
                  Srednja strukovna škola Antuna Horvata
                </Link>{" "}
                Đakovo - smjer Tehničar za mehatroniku
              </Typography>
            </ListItem>
          </List>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />
          <Typography
            style={{
              fontWeight: "bold",
              fontSize: "2.5vw",
              fontFamily: "Indie Flower",
            }}
          >
            Radno iskustvo i vještine:
          </Typography>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />
          <Typography style={{ fontSize: "1.5vw" }}>Iskustvo:</Typography>
          <List style={{ padding: 0, margin: 0 }}>
            <ListItem style={{ paddingTop: 4 }}>
              <Typography style={{ fontSize: "1.5vw" }}>
                Šest mjeseci radnog iskustva u tvrtki{" "}
                <Link href="https://www.marrowlabs.com">Marrow Labs</Link> kao
                front-end developer{" "}
              </Typography>
            </ListItem>
          </List>
          <Typography style={{ fontSize: "1.5vw" }}>Vještine:</Typography>
          <List style={{ padding: 0, margin: 0 }}>
            <ListItem style={{ paddingTop: 4 }}>
              <Typography style={{ fontSize: "1.5vw" }}>
                Vrlo dobro poznajem ReactJs i React Native programske jezike
              </Typography>
            </ListItem>
            <ListItem>
              <Typography style={{ fontSize: "1.5vw" }}>
                Poznavanje programskih jezika C, Python, Java, C#
              </Typography>
            </ListItem>

            <ListItem>
              <Typography style={{ fontSize: "1.5vw" }}>
                Vrlo dobro poznavanje Wordpressa
              </Typography>
            </ListItem>
            <ListItem>
              <Typography style={{ fontSize: "1.5vw" }}>
                Odlično služenje računalom i ostalim računalnim paketima
              </Typography>
            </ListItem>
            <ListItem>
              <Typography style={{ fontSize: "1.5vw" }}>
                Vrlo dobro poznavanje engleskog jezika
              </Typography>
            </ListItem>
          </List>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />{" "}
          <Typography
            style={{
              fontWeight: "bold",
              fontSize: "2.5vw",
              fontFamily: "Indie Flower",
            }}
          >
            Hobiji i interesi
          </Typography>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Izrada Web stranica
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            BlockChain
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Sviranje gitare
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Igranje igrica
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Vježbanje
          </Typography>
        </Card>
      </Box>
      <MediaEmanuel />
    </Box>
  );
};

export default Emanuel;
