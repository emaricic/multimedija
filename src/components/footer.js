import React from "react";
import { Box, Link, Typography } from "@mui/material";
import Logo from "../assets/images/logo.png";
const Footer = () => {
  return (
    <Box
      sx={{
        padding: "10px",
        marginTop: "40px",
        backgroundColor: "#143F6B",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
      }}
    >
      <Typography
        style={{
          alignSelf: "center",
          fontSize: "1.1vw",
          fontFamily: "Indie Flower",
          maxWidth: "1000px",
          marginLeft: "16px",
          marginRight: "16px",
          marginBottom: "8px",
          color: "white",
        }}
      >
        Web stranica je napravljena na{" "}
        <Link href="https://www.ferit.unios.hr/2021/">
          fakultetu elektrotehnike, računarstva i informacijskih tehnologija
        </Link>{" "}
        u Osijeku u sklopu kolegija{" "}
        <Link href="https://www.ferit.unios.hr/2021/upisi-i-studijski-programi/preddiplomski-strucni-studij#racunarstvo#SR601">
          Multimedijska tehnika.
        </Link>
      </Typography>
      <img
        src={Logo}
        style={{ width: "100%", maxWidth: "200px", height: "auto" }}
        alt=""
      />
    </Box>
  );
};

export default Footer;
