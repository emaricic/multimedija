import React, { useState } from "react";
import {
  Box,
  Typography,
  Divider,
  Card,
  Link,
  List,
  ListItem,
} from "@mui/material";

import MateoImg from "../assets/images/mateo.jpg";
import MateoInsta from "../assets/images/mateoinsta.bmp";
import MateoFacebook from "../assets/images/mateofacebook.gif";

import Modal from "react-modal";

import MediaMateo from "./mediaMateo";

const Mateo = () => {
  const [open, setOpen] = useState(false);
  const [openInsta, setOpenInsta] = useState(false);
  const [openFacebook, setOpenInstaFacebook] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleOpenInsta = () => setOpenInsta(true);
  const handleCloseInsta = () => setOpenInsta(false);
  const handleOpenFacebook = () => setOpenInstaFacebook(true);
  const handleCloseFacebook = () => setOpenInstaFacebook(false);

  return (
    <Box
      className="mateo"
      id="mateo"
      sx={{
        width: "100%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginTop: "20px",
        marginBottom: "20px",
      }}
    >
      <Typography
        style={{
          fontWeight: "bold",
          fontSize: "4vw",
          fontFamily: "Indie Flower",
          alignSelf: "center",
        }}
      >
        Mateo Jović
      </Typography>
      <Box
        variant="outlined"
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          flexWrap: "wrap",
          gap: "16px",
          marginBottom: "20px",
        }}
      >
        <Card
          sx={{
            flex: "1 1 200px",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            padding: "16px",
            boxShadow: 4,
          }}
        >
          <img
            onClick={handleOpen}
            src={MateoImg}
            style={{
              width: "100%",
              maxWidth: "400px",
              height: "auto",
              borderRadius: "8px",
            }}
            alt=""
          />
          <Modal
            closeTimeoutMS={200}
            contentLabel="Example Modal"
            isOpen={open}
            onRequestClose={handleClose}
            style={{
              content: {
                height: "fit-content",
                display: "flex",
                justifyContent: "center",
                backgroundColor: "rgb(0, 0, 0, 0.50)",
              },
              overlay: {
                backgroundColor: "rgb(0, 0, 0, 0.75)",
              },
            }}
          >
            <img
              style={{
                width: "100%",
                maxWidth: "500px",
                maxHeight: "600px",
                alignSelf: "center",
                borderRadius: "16px",
              }}
              src={MateoImg}
              alt=""
            />
          </Modal>

          <Box
            sx={{
              width: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "space-around",
              flexWrap: "wrap",
              pading: "8px",
            }}
          >
            <img
              onClick={handleOpenInsta}
              src={MateoInsta}
              style={{ width: "100%", maxWidth: "100px", height: "auto" }}
              alt=""
            />

            <Modal
              closeTimeoutMS={200}
              contentLabel="Example Modal"
              isOpen={openInsta}
              onRequestClose={handleCloseInsta}
              style={{
                content: {
                  height: "fit-content",
                  display: "flex",
                  justifyContent: "center",
                  backgroundColor: "rgb(0, 0, 0, 0.50)",
                },
                overlay: {
                  backgroundColor: "rgb(0, 0, 0, 0.75)",
                },
              }}
            >
              <img
                style={{
                  width: "100%",
                  maxWidth: "400px",
                  maxHeight: "400px",
                  alignSelf: "center",
                }}
                src={MateoInsta}
                alt=""
              />
            </Modal>
            <img
              onClick={handleOpenFacebook}
              src={MateoFacebook}
              style={{ width: "100%", maxWidth: "100px", height: "auto" }}
              alt=""
            />
            <Modal
              closeTimeoutMS={200}
              contentLabel="Example Modal"
              isOpen={openFacebook}
              onRequestClose={handleCloseFacebook}
              style={{
                content: {
                  height: "fit-content",
                  display: "flex",
                  justifyContent: "center",
                  backgroundColor: "rgb(0, 0, 0, 0.50)",
                },
                overlay: {
                  backgroundColor: "rgb(0, 0, 0, 0.75)",
                },
              }}
            >
              <img
                style={{
                  width: "100%",
                  maxWidth: "400px",
                  maxHeight: "400px",
                  alignSelf: "center",
                  borderRadius: "64px",
                }}
                src={MateoFacebook}
                alt=""
              />
            </Modal>
          </Box>
        </Card>

        <Card
          style={{
            boxShadow: 40,
            flex: "1 1 200px",
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            padding: "16px",
          }}
        >
          <Typography
            style={{
              fontWeight: "bold",
              fontSize: "2.5vw",
              fontFamily: "Indie Flower",
            }}
          >
            Informacije:
          </Typography>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Spol: muški
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Mjesto i datum rođenja:{" "}
            <Link href="https://www.osijek.hr">Osijek</Link>, 1. ožujka 2000.{" "}
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Mjesto prebivališta: <Link href="https://www.cepin.hr">Čepin</Link>,
            Hrvatska
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            E-mail adresa: mateojovic22@gmail.com
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Kontakt: 097 645 8691
          </Typography>
          <Typography style={{ fontSize: "1.5vw" }}>Obrazovanje:</Typography>
          <List style={{ padding: 0, margin: 0 }}>
            <ListItem style={{ paddingTop: 4 }}>
              <Typography style={{ fontSize: "1.5vw" }}>
                <Link href="http://os-mkrleze-cepin.skole.hr">
                  Osnovna škola Miroslava Krleže
                </Link>{" "}
                Čepin
              </Typography>
            </ListItem>
            <ListItem style={{ paddingTop: 4 }}>
              <Typography style={{ fontSize: "1.5vw" }}>
                <Link href="http://ss-ekonomska-upravna-os.skole.hr/skola/predmeti">
                  Ekonomska i upravna škola
                </Link>{" "}
                Osijek - smjer Upravni referent
              </Typography>
            </ListItem>
          </List>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />
          <Typography
            style={{
              fontWeight: "bold",
              fontSize: "2.5vw",
              fontFamily: "Indie Flower",
            }}
          >
            Radno iskustvo i vještine:
          </Typography>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />
          <Typography style={{ fontSize: "1.5vw" }}>Iskustvo:</Typography>
          <List style={{ padding: 0, margin: 0 }}>
            <ListItem style={{ paddingTop: 4 }}>
              <Typography style={{ fontSize: "1.5vw" }}>
                Trenutno obavljam praksu u tvrtki{" "}
                <Link href="https://atos.net/hr/hrvatska">Atos</Link> u Osijeku
              </Typography>
            </ListItem>
          </List>
          <Typography style={{ fontSize: "1.5vw" }}>Vještine:</Typography>
          <List style={{ padding: 0, margin: 0 }}>
            <ListItem style={{ paddingTop: 4 }}>
              <Typography style={{ fontSize: "1.5vw" }}>
                Poznavanje programskih jezika: Java, C i Python
              </Typography>
            </ListItem>
            <ListItem>
              <Typography style={{ fontSize: "1.5vw" }}>
                Odlično poznavanje SQL-a, Html-a, Css-a i solidno PHP-a
              </Typography>
            </ListItem>

            <ListItem>
              <Typography style={{ fontSize: "1.5vw" }}>
                Vrlo dobro služenje računalom
              </Typography>
            </ListItem>
            <ListItem>
              <Typography style={{ fontSize: "1.5vw" }}>
                Vrlo dobro poznavanje engleskog jezika
              </Typography>
            </ListItem>
            <ListItem>
              <Typography style={{ fontSize: "1.5vw" }}>
                Osnovno poznavanje njemačkog jezika
              </Typography>
            </ListItem>
          </List>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />{" "}
          <Typography
            style={{
              fontWeight: "bold",
              fontSize: "2.5vw",
              fontFamily: "Indie Flower",
            }}
          >
            Hobiji i interesi
          </Typography>
          <Divider
            flexItem
            style={{ marginTop: "16px", marginBottom: "16px" }}
          />
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Gledanje serija i filmova
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Nogomet
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Vježbanje
          </Typography>
          <Typography
            style={{
              fontFamily: "sans-serif",
              fontSize: "1.5vw",
            }}
          >
            Druženje
          </Typography>
        </Card>
      </Box>
      <MediaMateo />
    </Box>
  );
};

export default Mateo;
