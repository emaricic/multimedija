import React, { useState } from "react";
import { Box, Typography, Divider } from "@mui/material";
import Header from "./header";
import Footer from "./footer";
import Background from "../assets/images/background.png";

import Emanuel from "./emanuel";
import Mateo from "./mateo";
import Modal from "react-modal";

const Home = () => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: "100%",
        backgroundColor: "#FFFFFC",
      }}
    >
      <Header />

      <Box
        sx={{
          width: "100%",
          maxWidth: "1200px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          padding: "20px",
        }}
      >
        <Box
          sx={{
            flexGrow: 1,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            alignSelf: "center",
            marginTop: "20px",
            marginBottom: "20px",
            width: "100%",
          }}
        >
          <Typography style={{ fontSize: "2.6vw", fontFamily: "Indie Flower" }}>
            Dobrodošli na profile studenta Emanuela i Matea
          </Typography>
          <Box>
            <img
              onClick={handleOpen}
              style={{ width: "100%", maxWidth: "800px", height: "auto" }}
              src={Background}
              alt=""
            />
          </Box>
          <Divider
            flexItem
            style={{ marginTop: "20px", marginBottom: "20px" }}
          />
        </Box>

        <Emanuel id="emanuel" />
        <Mateo id="mateo" />
      </Box>
      <Footer />
      <Modal
        closeTimeoutMS={200}
        contentLabel="Example Modal"
        isOpen={open}
        onRequestClose={handleClose}
        style={{
          content: {
            height: "fit-content",
            display: "flex",
            justifyContent: "center",
            backgroundColor: "rgb(0, 0, 0, 0.50)",
          },
          overlay: {
            backgroundColor: "rgb(0, 0, 0, 0.75)",
          },
        }}
      >
        <img
          style={{
            width: "100%",
            maxWidth: "800px",
            maxHeight: "600px",
            alignSelf: "center",
          }}
          src={Background}
          alt=""
        />
      </Modal>
    </Box>
  );
};

export default Home;
