import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import GitHubIcon from "@mui/icons-material/GitHub";

const pages = [
  { id: 1, text: "EMANUEL", url: "#emanuel" },
  { id: 2, text: "MATEO", url: "#mateo" },
];
const settings = [
  { id: 1, uri: "https://gitlab.com/emaricic", text: "Emanuel" },
  { id: 2, uri: "https://gitlab.com/mjovic1", text: "Mateo" },
];

const colleges = [
  { id: 1, uri: "https://reli12.github.io/MMT_PublishPage/", text: "Kolega 1" },
  {
    id: 2,
    uri: "https://bernatovictisaiportfolio.netlify.app/",
    text: "Kolega 2",
  },
];

const Header = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [anchorElCollege, setAnchorElCollege] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleOpenCollegeMenu = (event) => {
    setAnchorElCollege(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleCloseCollegeMenu = () => {
    setAnchorElCollege(null);
  };

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ mr: 2, display: { xs: "none", md: "flex" } }}
          >
            <a
              style={{
                color: "white",
                textDecoration: "none",
              }}
              href="/"
            >
              PORTFOLIO
            </a>
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={handleCloseNavMenu}>
                  <Typography textAlign="center">
                    <a
                      style={{
                        color: "black",
                        textDecoration: "none",
                      }}
                      href={page.url}
                      key={page.id}
                    >
                      {page.text}
                    </a>
                  </Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}
          >
            <a
              style={{
                color: "white",
                textDecoration: "none",
              }}
              href="/"
            >
              PORTFOLIO
            </a>
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            {pages.map((page) => (
              <Button
                key={page.text}
                onClick={handleCloseNavMenu}
                sx={{ my: 2, color: "white", display: "block" }}
              >
                <a
                  style={{
                    color: "white",
                    textDecoration: "none",
                  }}
                  href={page.url}
                  key={page.id}
                >
                  {page.text}
                </a>
              </Button>
            ))}
            <Box sx={{ alignSelf: "center" }}>
              <Typography
                onClick={handleOpenCollegeMenu}
                sx={{
                  ml: 1,
                  fontSize: 14,
                  cursor: "pointer",
                }}
              >
                LINK NA KOLEGE
              </Typography>
              <Menu
                sx={{ mt: "45px" }}
                id="menu-appbar"
                anchorEl={anchorElCollege}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorElCollege)}
                onClose={handleCloseCollegeMenu}
              >
                {colleges.map((college) => (
                  <MenuItem key={college} onClick={handleCloseCollegeMenu}>
                    <Typography textAlign="center">
                      <a
                        style={{
                          color: "black",
                          textDecoration: "none",
                        }}
                        href={college.uri}
                        key={college.id}
                      >
                        {college.text}
                      </a>
                    </Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Github repo">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <GitHubIcon fontSize="large" />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: "45px" }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting) => (
                <MenuItem key={setting} onClick={handleCloseUserMenu}>
                  <Typography textAlign="center">
                    <a
                      style={{
                        color: "black",
                        textDecoration: "none",
                      }}
                      href={setting.uri}
                      key={setting.id}
                    >
                      {setting.text}
                    </a>
                  </Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Header;
