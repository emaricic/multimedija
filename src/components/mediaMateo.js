import React from "react";
import MateoVideo from "../assets/video/mateovideo.mp4";
import MateoAudio from "../assets/audio/mateoaudio.mp3";

import { Card } from "@mui/material";
import ReactPlayer from "react-player";
const MediaMateo = () => {
  return (
    <Card
      sx={{
        width: "100%",
        overflow: "hidden",
        padding: "16px",
        display: "flex",
        flexWrap: "wrap",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        gap: "16px",
      }}
    >
      <ReactPlayer
        style={{ maxWidth: "600px" }}
        controls
        width="100%"
        height={"40vw"}
        url={MateoVideo}
      />
      <ReactPlayer
        style={{ maxWidth: "600px" }}
        width="100%"
        height={"40px"}
        controls
        url={MateoAudio}
      />
    </Card>
  );
};

export default MediaMateo;
